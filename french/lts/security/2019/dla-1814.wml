#use wml::debian::translation-check translation="4849c2fe5ca8013dd25c736766c3c3050d48bbfd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de script intersite (XSS) dans le cadriciel de
développement Django.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12308">CVE-2019-12308</a>

<p>Un problème a été découvert dans Django 1.11 avant 1.11.21, 2.1 avant 2.1.9,
et 2.2 avant 2.2.2. La valeur cliquable d’URL actuel affichée par
AdminURLFieldWidget indique la valeur fournie sans valider que ce soit un URL
sûr. Par conséquent, une valeur non vérifiée stockée dans la base de données ou
une valeur fournie comme valeur de paramètre de requête d’URL, pourrait aboutir
à un lien JavaScript cliquable.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.11-1+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1814.data"
# $Id: $
