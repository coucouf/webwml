<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2012-6704">CVE-2012-6704</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-9793">CVE-2016-9793</a>

    <p>Eric Dumazet found that a local user with CAP_NET_ADMIN capability
    could set a socket's buffer size to be negative, leading to a
    denial of service or other security impact.  Additionally, in
    kernel versions prior to 3.5, any user could do this if sysctl
    net.core.rmem_max was changed to a very large value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1350">CVE-2015-1350</a> / #770492

    <p>Ben Harris reported that local users could remove set-capability
    attributes from any file visible to them, allowing a denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8962">CVE-2015-8962</a>

    <p>Calvin Owens fouund that removing a SCSI device while it was being
    accessed through the SCSI generic (sg) driver led to a double    
    free, possibly causing a denial of service (crash or memory
    corruption) or privilege escalation.  This could be exploited by
    local users with permision to access a SCSI device node.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8963">CVE-2015-8963</a>

    <p>Sasha Levin reported that hot-unplugging a CPU resulted in a
    use-after-free by the performance events (perf) subsystem,
    possibly causing a denial of service (crash or memory corruption)
    or privilege escalation.  This could by exploited by any local
    user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8964">CVE-2015-8964</a>

    <p>It was found that the terminal/serial (tty) subsystem did not
    reliably reset the terminal buffer state when the terminal line
    discipline was changed.  This could allow a local user with access
    to a terminal device to read sensitive information from kernel
    memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7097">CVE-2016-7097</a>

    <p>Jan Kara found that changing the POSIX ACL of a file never cleared
    its set-group-ID flag, which should be done if the user changing
    it is not a member of the group-owner.  In some cases, this would
    allow the user-owner of an executable to gain the privileges of
    the group-owner.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7910">CVE-2016-7910</a>

    <p>Vegard Nossum discovered that a memory allocation failure while
    handling a read of /proc/diskstats or /proc/partitions could lead
    to a use-after-free, possibly causing a denial of service (crash
    or memory corruption) or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7911">CVE-2016-7911</a>

    <p>Dmitry Vyukov reported that a race between ioprio_get() and
    ioprio_set() system calls could result in a use-after-free,
    possibly causing a denial of service (crash) or leaking sensitive
    information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7915">CVE-2016-7915</a>

    <p>Benjamin Tissoires found that HID devices could trigger an out-of    
    bounds memory access in the HID core.  A physically present user
    could possibly use this for denial of service (crash) or to leak
    sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8399">CVE-2016-8399</a>

    <p>Qidan He reported that the IPv4 ping socket implementation did
    not validate the length of packets to be sent.  A user with
    permisson to use ping sockets could cause an out-of-bounds read,
    possibly resulting in a denial of service or information leak.
    However, on Debian systems no users have permission to create ping
    sockets by default.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8633">CVE-2016-8633</a>

    <p>Eyal Itkin reported that the IP-over-Firewire driver
    (firewire-net) did not validate the offset or length in link-layer
    fragmentation headers.  This allowed a remote system connected by
    Firewire to write to memory after a packet buffer, leading to a
    denial of service (crash) or remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8645">CVE-2016-8645</a>

    <p>Marco Grassi reported that if a socket filter (BPF program)
    attached to a TCP socket truncates or removes the TCP header, this
    could cause a denial of service (crash).  This was exploitable by
    any local user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8655">CVE-2016-8655</a>

    <p>Philip Pettersson found that the implementation of packet sockets
    (AF_PACKET family) had a race condition between enabling a
    transmit ring buffer and changing the version of buffers used,
    which could result in a use-after-free.  A local user with the
    CAP_NET_ADMIN capability could exploit this for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9178">CVE-2016-9178</a>

    <p>Al Viro found that a failure to read data from user memory might
    lead to a information leak on the x86 architecture (amd64 or i386).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9555">CVE-2016-9555</a>

    <p>Andrey Konovalov reported that the SCTP implementation does not
    validate <q>out of the blue</q> packet chunk lengths early enough.  A
    remote system able could use this to cause a denial of service
    (crash) or other security impact for systems using SCTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9576">CVE-2016-9576</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2016-10088">CVE-2016-10088</a>

    <p>Dmitry Vyukov reported that using splice() with the SCSI generic
    driver led to kernel memory corruption.  Local users with
    permision to access a SCSI device node could exploit this for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9756">CVE-2016-9756</a>

    <p>Dmitry Vyukov reported that KVM for the x86 architecture (amd64 or
    i386) did not correctly handle the failure of certain instructions
    that require software emulation on older processors.  This could
    be exploited by guest systems to leak sensitive information or for
    denial of service (log spam).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9794">CVE-2016-9794</a>

    <p>Baozeng Ding reported a race condition in the ALSA (sound)
    subsystem that could result in a use-after-free.  Local users with
    access to a PCM sound device could exploit this for denial of
    service (crash or memory corruption) or other security impact.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.84-1.  This version also includes bug fixes from upstream version
3.2.84 and updates the PREEMPT_RT featureset to version 3.2.84-rt122.
Finally, this version adds the option to mitigate security issues in
the performance events (perf) subsystem by disabling use by
unprivileged users.  This can be done by setting sysctl
kernel.perf_event_paranoid=3.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.16.39-1 which will be included in the next point release (8.6).</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-772.data"
# $Id: $
